//
//  GoalCell.swift
//  GoalPost
//
//  Created by Todd Donnelly on 04/09/2017.
//  Copyright © 2017 Todd Donnelly. All rights reserved.
//

import UIKit

class GoalCell: UITableViewCell {

//outlets
    @IBOutlet weak var goalDescriptionLbl: UILabel!
    @IBOutlet weak var goalTypeLbl: UILabel!
    @IBOutlet weak var goalProgressLbl: UILabel!
    
    func configureCell(description:String, type:String, goalProgressAmount: Int){
        self.goalDescriptionLbl.text = description
        self.goalTypeLbl.text = type
        self.goalProgressLbl.text = String(describing: goalProgressAmount)
    }
    //comment
    
    
}
